package com.Tsvyatko.SimpleC.Parser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.Tsvyatko.SimpleC.Main.Node;
import com.Tsvyatko.SimpleC.tokenizer.Token;
import com.Tsvyatko.SimpleC.tokenizer.TokenType;
import com.Tsvyatko.SimpleC.tokenizer.Tokenizer;

public class Parser {

    private Tokenizer tokenizer;
    private Token token;
    private Token currentToken;
    private Node node;
    // private ArrayList<Node> compound;
    private String[] statements = {"{", "if", "while"};
    private String[] variables = {"int", "bool", "char"};
    private Map<String, String> variableSearch;
    private boolean isConst = false;

    // TODO Check for the right var type

    /**
     * Parser returns the Abstract Syntax Tree
     */
    public Parser(Tokenizer tokenizer) {
        variableSearch = new HashMap<String, String>();
        this.tokenizer = tokenizer;
        currentToken = tokenizer.ReadNextToken();
    }

    private Token getNextToken() {
        return tokenizer.ReadNextToken();
    }

    private Node compound_statement() {
        eat(TokenType.OPENCOMPOUND);
        node = statement_list();
        eat(TokenType.CLOSECOMPOUND);
        return node;
    }

    private Node statement_list() {
        ArrayList<Node> compoundList = new ArrayList<>();
        while (!(currentToken.getType() == TokenType.CLOSECOMPOUND)) {
            if(currentToken.getType() == TokenType.TYPE){
                compoundList.add(varDeclaration());
                eat(TokenType.SEMICOLON);
            }else if (currentToken.getType() == TokenType.FUNCTIONCREATE) {
                compoundList.add(funcDeclaration());
            }else{
                compoundList.add(statement());
                //eat(TokenType.SEMICOLON);
            }
        }
        return new Node(compoundList, new Token(0, 0, 0, "Compound", TokenType.COMPOUND));
    }

    private Node statement() {

        if (currentToken.getType() == TokenType.OPENCOMPOUND) {
            return compound_statement();
        }else if (currentToken.getType() == TokenType.WHILE) {
			return whileCall();
        }else if (currentToken.getType() == TokenType.DO) {
            return doWhileCall();
        }else if(currentToken.getType() == TokenType.IF){
            return ifCall();
            //node = fiCall();
            //if currentToken = else
            //elseCall() ?
        } else {
            node = Expression();
            if(node.getToken().getType() == TokenType.EMPTY){
                error("Statement");
            }
            eat(TokenType.SEMICOLON);
            return node;
        }
    }

    private Node Expression(){
        Node left = AdditiveExpr();
        if(left.getToken().getType() == TokenType.EMPTY){
            error("left Additive Expression");
        }
        Token t = getSpecialSymbols();
        if(t == null){
            return left;
        }
        Node right = AdditiveExpr();
        if(right.getToken().getType() == TokenType.EMPTY){
            error("right Additive Expression");
        }
        return new Node(left,t,right);
    }

    private Node PrimaryExrp() {
        token = currentToken;
        Node node = null;
        if(currentToken.getType() == TokenType.INTEGER_LITERAL){
            eat(TokenType.INTEGER_LITERAL);
            return new Node(token);
        }
        if(currentToken.getType() == TokenType.FUNCTIONCALL){
            return funcCall();
        }
        if(currentToken.getType() == TokenType.LPAREN){
            eat(TokenType.LPAREN);
            node = Expression();
            eat(TokenType.RPAREN);
            return node;
        }if(currentToken.getType() == TokenType.IDENTIFIER){
            node = getVariableIdent();
        }
        if (currentToken.getType() == TokenType.ASSIGN) {
            token = currentToken;
            eat(TokenType.ASSIGN);
    //        if (currentToken.getType() == TokenType.FUNCTIONCALL) {
    //            return new Node(node, currentToken, funcCall());
     //       }
            return new Node(node, token, Expression());
        } else if (currentToken.getType() == TokenType.PLUSEQUALSE) {
            eat(TokenType.PLUSEQUALSE);
            return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                    new Node(node, new Token(0, 0, 0, "+", TokenType.PLUS), Expression()));
        } else if (currentToken.getType() == TokenType.MINUSEQUALSE) {
            eat(TokenType.MINUSEQUALSE);
            return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                    new Node(node, new Token(0, 0, 0, "-", TokenType.MINUS), Expression()));
        } else if (currentToken.getType() == TokenType.MULEQUALSE) {
            eat(TokenType.MULEQUALSE);
            return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                    new Node(node, new Token(0, 0, 0, "*", TokenType.MUL), Expression()));
        } else if (currentToken.getType() == TokenType.DIVEQUALSE) {
            eat(TokenType.DIVEQUALSE);
            return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                    new Node(node, new Token(0, 0, 0, "/", TokenType.DIV), Expression()));
        }else if (currentToken.getType() == TokenType.REMEQUALE) {
                eat(TokenType.REMEQUALE);
               return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                        new Node(node, new Token(0, 0, 0, "/", TokenType.REM), Expression()));
        }
        if(node == null) {
            error(" Primary Expression! ");
        }
        return node;
    }

    private Node program() {
        node = statement();
        // eat(TokenType.SEMICOLON);
        return node;
    }

    private Node varDeclaration() {
        String type = currentToken.getValue();
        eat(TokenType.TYPE);
        Node left = getVariableIdent(type);
        if (isConst) {
            System.out.println("DOES THIS WOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOOORk");

            left.setConst();
            System.out.println("Node " + left.getTokenValue() + " CONST = " + left.isConst());
            isConst = false;
        }
        token = currentToken;
        if (token.getType() == TokenType.ASSIGN) {
            eat(TokenType.ASSIGN);
            return new Node(left, token, Expression());
        }
        return new Node(left, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                new Node(null, new Token(0, 0, 0, "EMPTY", TokenType.EMPTY), null));
        //this is for null
    }

    private Node funcDeclaration() {
        Token token = currentToken;
        eat(TokenType.FUNCTIONCREATE);
        String type = currentToken.getValue();
        eat(TokenType.TYPE);
        Node left = getVariableIdent(type);
        eat(TokenType.LPAREN);
        // ADD STUFF HERE
        eat(TokenType.RPAREN);
        node = new Node(left, token, compound_statement());
        System.out.println("LEFT " + left.getTokenValue() + " --- " + left.getType() + ", token is " + token.getValue()
                + " RIGHT: " + node.getRightNode().getTokenValue());
        return node;
    }

    private Node funcCall() {
        token = currentToken;
        eat(TokenType.FUNCTIONCALL);
        eat(TokenType.LPAREN);
        Node left = null;
        Node right = null;
        if(token.getValue().equals("printf") && currentToken.getType() == TokenType.QUOTE){
            eat(TokenType.QUOTE);
            left = new Node(new Token());
            right = new Node(new Node(new Token()),
                    new Token(0,0,0,currentToken.getValue(), currentToken.getType()), new Node(new Token())
                    );
            eat(TokenType.STRING);
            eat(TokenType.QUOTE);
            eat(TokenType.RPAREN);
            return new Node(left, token, right);
        }
        if (token.getValue().equals("printf")
                || token.getValue().equals("sqr")
                || token.getValue().equals("odd")
                || token.getValue().equals("abs")) {
            Token tokenP = token;
            left = new Node(new Token());

            right = Expression();
            eat(TokenType.RPAREN);

            //System.out.println("LEFT " + left.getTokenValue() + " --- " + left.getType() + ", token is " + token.getValue()
            //        + " RIGHT: " + right.getTokenValue());

            return new Node(left, tokenP, right);

        } else if (token.getValue().equals("scanf")) {
            eat(TokenType.RPAREN);//get line and oclumn
            return new Node(new Token(0, 0, 0, "scanf", TokenType.FUNCTIONCALL));
        }
        return null;
    }

    private Node ifCall(){
        Token tempToken = currentToken;
        eat(TokenType.IF);
        eat(TokenType.LPAREN);
        Node left = AdditiveExpr();
        Token t = getSpecialSymbols();
        if(t == null){
            error("No expression found in IF");
        }
        Node right = AdditiveExpr();
        Node expr = new Node(left,t,right);
        eat(TokenType.RPAREN);
        Node node = new Node(expr, tempToken, statement());
        if(currentToken.getType() == TokenType.ELSE){
            token = currentToken;
            eat(TokenType.ELSE);
            node.addElse(new Node(new Node(new Token()), token, statement()));
        }
        return node;
    }

    private Node whileCall(){
        Token tempToken = currentToken;
        eat(TokenType.WHILE);
        eat(TokenType.LPAREN);
        if(currentToken.getType() == TokenType.OPOSITE){
            Token opToken = currentToken;
            eat(TokenType.OPOSITE);
            eat(TokenType.LPAREN);
            Node left = AdditiveExpr();
            Token t = getSpecialSymbols();
            if(t == null){
                error("No expression found in WHILE");
            }
            Node right = AdditiveExpr();
            eat(TokenType.RPAREN);
            eat(TokenType.RPAREN);
            Node expr = new Node(left,t,right);
            Node opNode = new Node(expr,opToken,statement());
            return new Node(new Node(new Token()), tempToken, opNode);
        }else{
            Node left = AdditiveExpr();
            Token t = getSpecialSymbols();
            if(t == null){
                error("No expression found in WHILE");
            }
            Node right = AdditiveExpr();
            Node expr = new Node(left,t,right);
            eat(TokenType.RPAREN);
            return new Node(expr, tempToken, statement());
        }
    }

    private Node doWhileCall(){
        Token tempToken = currentToken;
        eat(TokenType.DO);
        Node leftWhile = statement();
        eat(TokenType.WHILE);
        eat(TokenType.LPAREN);
        Node left = AdditiveExpr();
        Token t = getSpecialSymbols();
        if(t == null){
            error("No expression found in WHILE");
        }
        Node right = AdditiveExpr();
        Node expr = new Node(left,t,right);
        eat(TokenType.RPAREN);
        eat(TokenType.SEMICOLON);
        Node node = new Node(expr, tempToken, leftWhile);
        return node;
    }

    private Token getSpecialSymbols(){
        token = currentToken;
        if(currentToken.getType() == TokenType.LESS){
            eat(TokenType.LESS);
            return token;
        }else if(currentToken.getType() == TokenType.GREATER){
            eat(TokenType.GREATER);
            return token;
        }else if(currentToken.getType() == TokenType.LESSEQUALE){
            eat(TokenType.LESSEQUALE);
            return token;
        }else if(currentToken.getType() == TokenType.GREATEREQUALE){
            eat(TokenType.GREATEREQUALE);
            return token;
        }else if(currentToken.getType() == TokenType.NOTEQUALE){
            eat(TokenType.NOTEQUALE);
            return token;
        }else if(currentToken.getType() == TokenType.EQUALEEQUALE){
            eat(TokenType.EQUALEEQUALE);
            return token;
        }else if(currentToken.getType() == TokenType.OPOSITE){
            eat(TokenType.OPOSITE);
            return token;
        }else if(currentToken.getType() == TokenType.OR){
            eat(TokenType.OR);
            return token;
        }
        return null;
    }

    private Node getVariableIdent(String type) {
        int line = currentToken.getLine();
        int column = currentToken.getColumn();
        Node node = new Node(new Token(line, column, 0, currentToken.getValue(), currentToken.getType()));
        node.setType(type);
        eat(TokenType.IDENTIFIER);
        return node;
    }

    private Node getVariableIdent() {
        int line = currentToken.getLine();
        int column = currentToken.getColumn();
        Node node = new Node(new Token(line, column, 0, currentToken.getValue(), currentToken.getType()));
        eat(TokenType.IDENTIFIER);
        return node;
    }

    private Node SimpleExpr() {
        Node node = PrimaryExrp();
        /*
        probrai ytre po scemata, proveri parvo kakto e currentToken, ako e ++ ili -- ili -, napravi left da e 0, token da e current token i right da e primaryExper;
        else node = primary expr, sled tova proveri sa ++ i -- i return;

        * */
        if (node.getToken().getType() == TokenType.EMPTY) {
            error("SimpleExpr");
        }
        if (currentToken.getType() == TokenType.PLUSPLUS) {
            eat(TokenType.PLUSPLUS);
            if(node.getToken().getType() == TokenType.INTEGER_LITERAL) {
                return new Node(node, new Token(0, 0, 0, "+", TokenType.PLUS),
                        new Node(new Token(0, 0, 0, "1", TokenType.INTEGER_LITERAL)));
            }else if(node.getToken().getType() == TokenType.IDENTIFIER){
                return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                        new Node(node, new Token(0, 0, 0, "+", TokenType.PLUS),
                                new Node(new Token(0, 0, 0, "1", TokenType.INTEGER_LITERAL))));
            }
        } else if (currentToken.getType() == TokenType.MINUSMINUS) {
            eat(TokenType.MINUSMINUS);
            if(node.getToken().getType() == TokenType.INTEGER_LITERAL) {
                return new Node(node, new Token(0, 0, 0, "-", TokenType.MINUS),
                        new Node(new Token(0, 0, 0, "1", TokenType.INTEGER_LITERAL)));
            }else if(node.getToken().getType() == TokenType.IDENTIFIER){
                return new Node(node, new Token(0, 0, 0, "=", TokenType.ASSIGN),
                        new Node(node, new Token(0, 0, 0, "-", TokenType.MINUS),
                                new Node(new Token(0, 0, 0, "1", TokenType.INTEGER_LITERAL))));
            }
        }
        return node;
    }
    /*

        if (token.getType() == TokenType.PLUS) {
            eat(TokenType.PLUS);
            return new Node(new Node(new Token(0, 0, 0, "0", TokenType.INTEGER_LITERAL)), token, factor());
        } else if (token.getType() == TokenType.MINUS) {
            eat(TokenType.MINUS);
            return new Node(new Node(new Token(0, 0, 0, "0", TokenType.INTEGER_LITERAL)), token, factor());
        } else if (token.getType() == TokenType.FUNCTIONCALL) {
            return funcCall();
        }
        /*
         * tova trqbva da go napravi6 za --b ili minusminus identificator else if
         * (token.getType() == TokenType.PLUSPLUS) { eat(TokenType.PLUSPLUS); return new
         * Node(variable(), new Token(0, 0, 0, "+", TokenType.PLUS), new Node(new
         * Token(0, 0, 0, "1", TokenType.INTEGER_LITERAL))); // tuk b nqma da ima type
         * 6oto ne se znae dali e int double za da se izvika drugiq metod }

        else if (token.getType() == TokenType.INTEGER_LITERAL) {
            eat(TokenType.INTEGER_LITERAL);
            return new Node(token);
        } else if (token.getType() == TokenType.DOUBLE_LITERAL) {
            eat(TokenType.DOUBLE_LITERAL);
            return new Node(token);
        } else if (token.getType() == TokenType.LPAREN) {
            eat(TokenType.LPAREN);
            node = AdditiveExpr();
            eat(TokenType.RPAREN);
            return node;
        } else if (token.getType() == TokenType.IDENTIFIER) {
            return variable(); // tuk b nqma da ima type 6oto ne se znae dali e int double za da se izvika
            // drugiq metod
        } else {
            int line = currentToken.getLine();
            int column = currentToken.getColumn();
            return new Node(new Token(line, column, 0, "EMPTY", TokenType.EMPTY));
            // variable(); // FIX ?
        }
    }
*/
    private Node MultiplicativeExpr() {
        Node node = SimpleExpr();
        if(node.getToken().getType() == TokenType.EMPTY){
            error("Simple Expression");
        }
        while (currentToken.getType() == TokenType.MUL || currentToken.getType() == TokenType.DIV
                || currentToken.getType() == TokenType.DIV_INT || currentToken.getType() == TokenType.REM) {
            Token tempToken = currentToken;
            // TODO make a difference between integer and double
            if (tempToken.getType() == TokenType.MUL) {
                eat(TokenType.MUL);
            } else if (tempToken.getType() == TokenType.DIV) {
                eat(TokenType.DIV);
            } else if (tempToken.getType() == TokenType.DIV_INT) {
                eat(TokenType.DIV_INT);
            } else if (tempToken.getType() == TokenType.REM) {
                eat(TokenType.REM);
            }
            node = new Node(node, tempToken, SimpleExpr());
        }
        // System.out.println("Node with left: " + node.getLeftNode() + " op: \"" +
        return node;
    }

    private Node AdditiveExpr() {
        Node node = null;
  //      if(currentToken.getType() == TokenType.PLUS || currentToken.getType() == TokenType.MINUS){
 //           node = new Node(new Node(new Token()), currentToken, MultiplicativeExpr());
 //       }
        if(currentToken.getType() == TokenType.PLUS || currentToken.getType() == TokenType.MINUS){
            node = new Node(new Token(0,0,0,"0", TokenType.INTEGER_LITERAL));
        }else {
            node = MultiplicativeExpr();
        }
        //currentToken.getType();
        while (currentToken.getType() == TokenType.PLUS || currentToken.getType() == TokenType.MINUS) {
            Token tempToken = currentToken;
            if (tempToken.getType() == TokenType.PLUS) {
                eat(TokenType.PLUS);
            } else if (tempToken.getType() == TokenType.MINUS) {
                eat(TokenType.MINUS);
            }
            Node right = AdditiveExpr();
            /// MOJE DA GARMI ---------------------------------------------------------------------------------------------
            if(right.getToken().getType() == TokenType.EMPTY){
                error(" has " + tempToken + " but no right token!");
            }
            return new Node(node, tempToken, right);
        }
        return node;
    }

    public Node parse() {
        node = program();
        if (currentToken.getType() != TokenType.EOF) {
            error(TokenType.EOF);
        }
        return node;
    }

    private boolean isStatement(String a) {
        for (String s : statements) {
            if (s.equals(a)) {
                return true;
            }
        }
        return false;
    }

    private void eat(TokenType type) {
        if (currentToken.getType() == type) {
            currentToken = getNextToken();
        } else {
            error(type);
        }
    }

    private void error(TokenType type) {
        throw new IllegalArgumentException("Invalid syntax, expected " + type + " at column: " + currentToken.getColumn()
                + " line: " + currentToken.getLine() + " got " + currentToken.getValue());
    }

    private void error(String s) {
        throw new IllegalArgumentException("Invalid syntax, missing a " + s + " at column: " + currentToken.getColumn()
                + " line: " + currentToken.getLine());
    }

    private boolean isVariable(String a) {
        for (String s : variables) {
            if (s.equals(a)) {
                return true;
            }
        }
        return false;
    }
}
