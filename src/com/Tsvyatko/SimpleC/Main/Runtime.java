package com.Tsvyatko.SimpleC.Main;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;


import com.Tsvyatko.SimpleC.Compiler.Compiler;
import com.Tsvyatko.SimpleC.Parser.Parser;
import com.Tsvyatko.SimpleC.SymbolTable.SemanticAnalyzer;
import com.Tsvyatko.SimpleC.tokenizer.Token;
import com.Tsvyatko.SimpleC.tokenizer.Tokenizer;

public class Runtime {

	private ArrayList<Token> tokens;
	private Node AST;
	private SemanticAnalyzer table;

	public Runtime(){
		tokens = new ArrayList<>();
		table = new SemanticAnalyzer();
		table.print();
		String nameOfFile = "Example1.txt";
		String line = null;
		String code = "";

		try {
			FileReader fr = new FileReader("src\\Examples\\" + nameOfFile);
			BufferedReader br = new BufferedReader(fr);

			while((line = br.readLine()) != null){
				code = code + line + "\n";
			}

			br.close();
		}catch (FileNotFoundException ex){
			System.out.println(
					"Unable to open file '" +
							nameOfFile + "'");
		}catch (IOException ex){
			System.out.println(
					"Error reading file '"
							+ nameOfFile + "'");
		}
		//System.out.println("FILE............................................................");
		//System.out.println(code);
		//System.out.println("END............................................................");

		Tokenizer tokenizer = new Tokenizer(code);
		Parser parser = new Parser(tokenizer);
		AST = parser.parse();
		table.visitProgram(AST);
		table.print();

		nameOfFile = nameOfFile.replace(".txt", "");
		Compiler c = new Compiler(AST, nameOfFile);
		c.compileBegin();
	}

	public static void main(String[] args) {
		new Runtime();
	}

}
