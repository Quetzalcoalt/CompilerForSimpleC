package com.Tsvyatko.SimpleC.Main;
import java.util.ArrayList;

import com.Tsvyatko.SimpleC.tokenizer.Token;

public class Node {
	
	private Node left;
	private Token token;
	private Node right;
	private ArrayList<Node> nodeList;
	private String type = "";
	private boolean isConst = false;
	private Node ifElse;
	
	public Node(Node left, Token token, Node right) {
		this.left = left;
		this.token = token;
		this.right = right;
	}
	
	public Node(ArrayList<Node> list, Token token) {
		this.nodeList = list;
		this.token = token;
	}
	
	public Node(Token token) {
		this.left = null;
		this.token = token;
		this.right = null;
	}

	public void addElse(Node node){
		ifElse = node;
	}

	public Node getElse(){
		return ifElse;
	}
	
	public Node getLeftNode() {
		return left;
	}
	
	public Token getToken() {
		return token;
	}
	
	public String getTokenValue() {
		return token.getValue();
	}
	
	public Node getRightNode() {
		return right;
	}
	
	public ArrayList<Node> getArrayList(){
		return nodeList;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}

	public void setConst(){
		isConst = true;
	}

	public boolean isConst() {
		return isConst;
	}
}
