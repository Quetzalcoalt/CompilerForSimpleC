package com.Tsvyatko.SimpleC.tokenizer;

public enum TokenType {
	
	EMPTY,
	
	NULL,
	
	OPENCOMPOUND,
	CLOSECOMPOUND,
	OPENBRACKET,
	CLOSEBRACKET,
	
	
	PLUS,MINUS,DIV,MUL,LPAREN,RPAREN,EOF,ASSIGN,DIV_INT,

	COMPOUND,FUNCTIONCALL,FUNCTIONCREATE, VOID, CONST, IF, ELSE, WHILE, DO,

	LESS,GREATER,LESSEQUALE,GREATEREQUALE,NOTEQUALE,EQUALEEQUALE,OR,OPOSITE,
	
	PLUSEQUALSE,MINUSEQUALSE,MULEQUALSE,DIVEQUALSE, PLUSPLUS, MINUSMINUS, REM, REMEQUALE,
	
	SEMICOLON, QUOTE, STRING,
	
	/** Reserved words like class, void int double if while ... */
	KEYWORD,
	
	/** Operators are = + - *\/         */
	OPERATOR,
	
	
	
	
	/** First character is a letter, after that any length of letters or numbers, for ex. names of variables, ect*/
	IDENTIFIER,
	
	/**VARIABLE TYPES */
	//INTEGER,DOUBLE,CHAR,BOOLEAN,
	TYPE,
	
	/** A number*/
	INTEGER_LITERAL,
	
	/** A number with a .*/
	DOUBLE_LITERAL,
	
	BOOLEAN_LITERAL,
	
	CHAR_LITERAL,
	
	/** anything that is inside quotes. "text + 1 = $" */
	STRING_LITERAL
}
