package com.Tsvyatko.SimpleC.tokenizer;

import java.util.ArrayList;

public class Tokenizer {

	private String token;
	private Token tok;
	private ArrayList<Character> chars;
	private int charIterator;
	private int line;
	private int column;
	private int tokenLenght;
	public boolean endOfFile = false;
	//private String[] keyWords = { "if", "else", "while", "break", "continue" };
	private String[] variables = { "int", "bool", "char" };
	char currentChar;
	boolean dummyChar = false;
	boolean dummyChar2 = false;
	boolean quote = false;
	boolean foundString = false;

	public Tokenizer(String str) {
		chars = new ArrayList<>();
		charIterator = 0;
		line = 1;
		column = 0;
		tokenLenght = 1;
		endOfFile = false;
		convertStringToCharArray(str);
		currentChar = chars.get(charIterator);
	}
	public Token ReadNextToken() {
		tokenLenght = 1;
		token = "";
		tok = null;
		column++;
		while (currentChar != '\u0000') {

			if(quote){
				String name = getString();
				tok = new Token(line, column, tokenLenght, name, TokenType.STRING);
				quote = false;
				foundString = true;
				break;
			}

			if (isSpace()) {
				skip_whitespace();
				column++;
				continue;
			}
			if(currentChar == '\n'){
				advance();
				line++;
				column = 1;
				continue;
			}
			if(currentChar == '\t') {
				advance();
				continue;
			}
			if(currentChar == '\\' && peek() == 'n'){
				advance();
				advance();
				continue;
			}
			if (Character.isAlphabetic(currentChar)) {
				String name = getID();
				if(name.equals("int")) { // make one if <--------------------------------
					tok = new Token(line, column, tokenLenght, name, TokenType.TYPE);
					column += 2;
				}else if(name.equals("double")){
					tok = new Token(line, column, tokenLenght, name, TokenType.TYPE);
					column += 5;
				//}else if(name.equals("char")) {
				//	tok = new Token(line, column, tokenLenght, name, TokenType.TYPE);
				//}else if(name.equals("bool")) {
				//	tok = new Token(line, column, tokenLenght, name, TokenType.TYPE);
				}else if(name.equals("div")){
					tok = new Token(line,column,tokenLenght,name,TokenType.DIV_INT);
				}else if(name.equals("function")){
					tok = new Token(line,column,tokenLenght,name,TokenType.FUNCTIONCREATE);
					column += 7;
				}else if(name.equals("printf")){
					tok = new Token(line,column,tokenLenght,name,TokenType.FUNCTIONCALL);
					column += 5;
				}else if( name.equals("scanf")){
					tok = new Token(line,column,tokenLenght,name,TokenType.FUNCTIONCALL);
					column += 4;
				}else if(name.equals("sqr")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.FUNCTIONCALL);
					column += 2;
				}else if(name.equals("abs")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.FUNCTIONCALL);
					column += 2;
				}else if(name.equals("odd")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.FUNCTIONCALL);
					column += 2;
				}else if(name.equals("void")){
					tok = new Token(line,column,tokenLenght,name,TokenType.TYPE);
					column += 3;
				}else if(name.equals("if")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.IF);
					column += 1;
				}else if(name.equals("else")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.ELSE);
					column += 3;
				}else if(name.equals("while")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.WHILE);
					column += 4;
				}else if(name.equals("do")) {
					tok = new Token(line, column, tokenLenght, name, TokenType.DO);
					column += 1;
				}else if(name.equals("CONST-")) {
					//THIS DOESN NOT WORK
					tok = new Token(line, column, tokenLenght, name, TokenType.CONST);
				}else {
					tok = new Token(line, column, tokenLenght, name, TokenType.IDENTIFIER);
				}
				break;
			}
			if(currentChar == '"'){
				if(!quote && foundString){
					foundString = false;
				}else if(!quote && !foundString){
					quote = true;
				}
				tok = new Token(line, column, tokenLenght, "\"", TokenType.QUOTE);
				advance();
				break;

			}
			if(currentChar == '<'){
				if(peek() == '='){
					tok = new Token(line, column, tokenLenght, "<=", TokenType.LESSEQUALE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, "<", TokenType.LESS);
				advance();
				break;
			}
			if(currentChar == '>'){
				if(peek() == '='){
					tok = new Token(line, column, tokenLenght, ">=", TokenType.GREATEREQUALE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, ">", TokenType.GREATER);
				advance();
				break;
			}
			if(currentChar == '!'){
				if(peek() == '='){
					tok = new Token(line, column, tokenLenght, "!=", TokenType.NOTEQUALE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, "!", TokenType.OPOSITE);
				advance();
				break;
			}
			if(currentChar == '|'){
				tok = new Token(line, column, tokenLenght, "|", TokenType.OR);
				advance();
				break;
			}
			if (Character.isDigit(currentChar)) {
				tok = getNumber();
				dummyChar2 = true;
				break;
			}
			if (currentChar == '{') {
				tok = new Token(line, column, tokenLenght, "{", TokenType.OPENCOMPOUND);
				advance();
				break;
			}
			if(currentChar == '}') {
				tok = new Token(line, column, tokenLenght, "}", TokenType.CLOSECOMPOUND);
				advance();
				break;
			}
			if(currentChar == '=') {
				if(peek() == '='){
					tok = new Token(line, column, tokenLenght, "==", TokenType.EQUALEEQUALE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, "=", TokenType.ASSIGN);
				advance();
				break;
			}
			if(currentChar == ';') {
				tok = new Token(line, column, tokenLenght, ";", TokenType.SEMICOLON);
				advance();
				break;
			}
			if (currentChar == '+') {
				if(peek() == '=') {
					tok = new Token(line, column, tokenLenght, "+=", TokenType.PLUSEQUALSE);
					advance();
					advance();
					break;
				}
				if(peek() == '+') {
					if(Character.isDigit(peek(2))) {
						tok = new Token(line, column, tokenLenght, "+", TokenType.PLUS);
						advance();
						break;
					}if(peek(2) == '+'){
						dummyChar = true;
						System.out.println("DummyChar is now " + dummyChar);
						tok = new Token(line, column, tokenLenght, "+", TokenType.PLUS);
						advance();
						break;
					}
					if(Character.isAlphabetic(peek(2))) {
						if (!dummyChar) {
							tok = new Token(line, column, tokenLenght, "++", TokenType.PLUSPLUS);
							advance();
							advance();
							break;
						}else {
							dummyChar = false;
							tok = new Token(line, column, tokenLenght, "+", TokenType.PLUS);
							advance();
							break;
						}
					}
					tok = new Token(line, column, tokenLenght, "++", TokenType.PLUSPLUS);
					advance();
					advance();
					break;

				}
				tok = new Token(line, column, tokenLenght, "+", TokenType.PLUS);
				advance();
				break;
			}
			if (currentChar == '-') {
				if(peek() == '=') {
					tok = new Token(line, column, tokenLenght, "-=", TokenType.MINUSEQUALSE);
					advance();
					advance();
					break;
				}
				if(peek() == '-') {
					if(Character.isDigit(peek(2))) {
						tok = new Token(line, column, tokenLenght, "-", TokenType.MINUS);
						advance();
						break;
					}if(peek(2) == '-'){
						dummyChar = true;
						System.out.println("DummyChar is now " + dummyChar);
						tok = new Token(line, column, tokenLenght, "-", TokenType.MINUS);
						advance();
						break;
					}
					if(Character.isAlphabetic(peek(2))) {
						if (!dummyChar) {
							tok = new Token(line, column, tokenLenght, "--", TokenType.MINUSMINUS);
							advance();
							advance();
							break;
						}else {
							dummyChar = false;
							tok = new Token(line, column, tokenLenght, "-", TokenType.MINUS);
							advance();
							break;
						}
					}
					tok = new Token(line, column, tokenLenght, "--", TokenType.MINUSMINUS);
					advance();
					advance();
					break;

				}
				tok = new Token(line, column, tokenLenght, "-", TokenType.MINUS);
				advance();
				break;
			}
			if (currentChar == '*') {
				if(peek() == '=') {
					tok = new Token(line, column, tokenLenght, "*=", TokenType.MULEQUALSE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, "*", TokenType.MUL);
				advance();
				break;
			}
			if (currentChar == '/') {
				if(peek() == '=') {
					tok = new Token(line, column, tokenLenght, "/=", TokenType.DIVEQUALSE);
					advance();
					advance();
					break;
				}
				if(peek() == '/') {
					line++;
					skip_comment();
					continue;
				}
				tok = new Token(line, column, tokenLenght, "/", TokenType.DIV);
				advance();
				break;
			}
			if (currentChar == '%') {
				if(peek() == '=') {
					tok = new Token(line, column, tokenLenght, "/=", TokenType.REMEQUALE);
					advance();
					advance();
					break;
				}
				tok = new Token(line, column, tokenLenght, "%", TokenType.REM);
				advance();
				break;
			}
			if (currentChar == '(') {
				tok = new Token(line, column, tokenLenght, "(", TokenType.LPAREN);
				advance();
				break;
			}
			if (currentChar == ')') {
				tok = new Token(line, column, tokenLenght, ")", TokenType.RPAREN);
				advance();
				break;
			}
			error();
		}
		if (tok == null) {
			System.out.println("CURRENT column: " + column + " and line: " + line);
			return new Token(line, column, tokenLenght, "EOF", TokenType.EOF);
		} else {
			System.out.println("CURRENT column: " + tok.getColumn() + " and line: " + tok.getLine());
			System.out.println("Token " + tok.getValue() + " " + tok.getType());
			return tok;
		}
	}
	
	private void skip_comment() {
		while(true) {
			//System.out.println("skip: " + currentChar);
			if(currentChar == '\n' || currentChar=='\u0000') {
				break;
			}
			advance();
		}
		advance();
	}

	private String getID() {
		String result = "";
		while (currentChar != '\u0000' && (Character.isAlphabetic(currentChar) || Character.isDigit(currentChar))) {
			result += currentChar;
			advance();
			if(isVariable(result)) {
				return result;
			}
		}
		return result;
	}

	private String getString() {
		String result = "";
		while (currentChar != '\u0000' && !(currentChar == '"')){
		//	if(currentChar == '\"'){
		//		break;
		//	}
			result += currentChar;
			advance();
		}
		return result;
	}

	private void error() {
		throw new IllegalArgumentException("Invalid character " + currentChar + " at column: " + column + " line: " + line);
	}

	private void advance() {
		charIterator++;
		if (charIterator > chars.size() - 1) {
			currentChar = '\u0000';
		} else {
			currentChar = chars.get(charIterator);
		}
	}

	private void skip_whitespace() {
		while (currentChar != '\u0000' && isSpace()) {
			advance();
		}
	}

	private boolean isSpace() {
		if (currentChar == ' ') {
			return true;
		}
		return false;
	}

	private boolean isDigit() {
		if (currentChar == '0' || currentChar == '1' || currentChar == '2' || currentChar == '3' || currentChar == '4'
				|| currentChar == '5' || currentChar == '6' || currentChar == '7' || currentChar == '8'
				|| currentChar == '9') {
			return true;
		} else {
			return false;
		}
	}

	private Token getNumber() {
		String result = "";
		Token tokenNumber;
		while (currentChar != '\u0000' && isDigit()) {
			result += currentChar;
			advance();
		}
		if(currentChar=='.') {
			result += currentChar;
			advance();
			
			if(peek() == '.') {
				error();
			}
			while (currentChar != '\u0000' && isDigit()) {
				result += currentChar;
				advance();
			}
			tokenNumber = new Token(line, column, tokenLenght, result, TokenType.DOUBLE_LITERAL);
		}else {
			tokenNumber = new Token(line, column, tokenLenght, result, TokenType.INTEGER_LITERAL);
		}
		
		return tokenNumber;
	}

	private char peek() {
		int peek = charIterator;
		peek++;
		if (peek > chars.size() - 1) {
			return '\u0000';
		} else {
			return chars.get(peek);
		}
	}
	private char peek(int a) {
		int peek = charIterator;
		peek += a;
		if (peek > chars.size() - 1) {
			return '\u0000';
		} else {
			return chars.get(peek);
		}
	}

	private boolean isVariable(String token) {
		for (String s : variables) {
			if (token.equals(s)) {
				return true;
			}
		}
		return false;
	}

	private void convertStringToCharArray(String string) {
		string.toLowerCase();
		for (int i = 0; i < string.length(); i++) {
			chars.add(string.charAt(i));
		}
		// System.out.println(chars.toString());
	}
}
