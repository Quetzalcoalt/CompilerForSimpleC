package com.Tsvyatko.SimpleC.tokenizer;

public class Token{

	private int line;
	private int column;
	private int endColumn;
	private String value;
	private TokenType type;
	
	public Token(int line, int column, int length, String name, TokenType type) {
		this.line = line;
		this.column = column;
		this.endColumn = this.line - length;
		this.value = name;
		this.type = type;
	}

	public Token(){
		line = 0;
		column = 0;
		endColumn = 0;
		value = "EMPTY";
		type = TokenType.EMPTY;
	}

	public TokenType getType() {
		return type;
	}
	
	public String getValue() {
		return value;
	}

	public int getColumn() {
		return column;
	}

	public int getLine() {
		return line;
	}
}
