package com.Tsvyatko.SimpleC.tokenizer;

public class Type {

	String name;
	String type;
	
	public Type(String name, String type) {
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}
	
	
}
