package com.Tsvyatko.SimpleC.SymbolTable;

import com.Tsvyatko.SimpleC.Main.Node;
import com.Tsvyatko.SimpleC.tokenizer.Token;
import com.Tsvyatko.SimpleC.tokenizer.TokenType;

public class SemanticAnalyzer {
    ScopedSymbolTable scope;
    boolean isConst = false;

    public SemanticAnalyzer() {
        this.scope = new ScopedSymbolTable("global", 1);
    }

    private void visitOperator(Node node) {
        visitExpression(node.getLeftNode());
        visitExpression(node.getRightNode());
    }

    private void visitStatement(Node node) {
        Token token = node.getToken();
        if (token == null) {
            throw new IllegalStateException("node token is NULL!");
        }
        if (token.getType() == TokenType.COMPOUND) {
            visitCompound(node);
        } else if (token.getType() == TokenType.IF) {
            visitIf(node);
        } else if (token.getType() == TokenType.WHILE) {
            visitWhile(node);
        } else if (token.getType() == TokenType.DO) {
            visitDoWhile(node);
        } else {
            visitExpression(node);
        }
    }

    private void visitWhile(Node node){
        if(node.getRightNode().getToken().getType() == TokenType.OPOSITE){
            visitExpression(node.getRightNode().getLeftNode());
            visitStatement(node.getRightNode().getRightNode());
        }else {
            visitExpression(node.getLeftNode());
            visitStatement(node.getRightNode());
        }
    }

    private void visitDoWhile(Node node){
       // visitExpression(node.getLeftNode().getLeftNode());
        //visitStatement(node.getRightNode().getRightNode());
    }

    private void visitNumber(Node node) {

    }

    private void visitIf(Node node) {
        visitExpression(node.getLeftNode());
        visitStatement(node.getRightNode());
        System.out.println("asd");
    }

    private void visitExpression(Node node) {
        Token token = node.getToken();
        if (token.getType() == TokenType.LESS
                || token.getType() == TokenType.GREATER
                || token.getType() == TokenType.LESSEQUALE
                || token.getType() == TokenType.GREATEREQUALE
                || token.getType() == TokenType.EQUALEEQUALE
                || token.getType() == TokenType.NOTEQUALE
                || token.getType() == TokenType.OPOSITE) {
            visitAdditiveExpression(node.getLeftNode());
            visitAdditiveExpression(node.getRightNode());
        } else {
            visitAdditiveExpression(node);
        }
    }

    private void visitAdditiveExpression(Node node) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.PLUS) {
            visitMultiplicativeExpression(node.getLeftNode());
            visitMultiplicativeExpression(node.getRightNode());
        } else if (t == TokenType.MINUS) {
            visitMultiplicativeExpression(node.getLeftNode());
            visitMultiplicativeExpression(node.getRightNode());
        } else {
            visitMultiplicativeExpression(node);
        }

    }

    private void visitMultiplicativeExpression(Node node) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.MUL) {
            visitSimpleExpression(node.getLeftNode());
            visitSimpleExpression(node.getRightNode());
        } else if (t == TokenType.DIV) {
            visitSimpleExpression(node.getLeftNode());
            visitSimpleExpression(node.getRightNode());
        } else if (t == TokenType.REM) {
            visitSimpleExpression(node.getLeftNode());
            visitSimpleExpression(node.getRightNode());
        } else {
            visitSimpleExpression(node);
        }
    }

    private void visitSimpleExpression(Node node) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.ASSIGN) {
            visitAssign(node);
        } else {
            visitPrimaryExpression(node);
        }
    }

    private void visitPrimaryExpression(Node node) {
        Token token = node.getToken();
        TokenType t = token.getType();
        //if const,
        if (t == TokenType.INTEGER_LITERAL) {
            visitNumber(node);
        } else if (t == TokenType.IDENTIFIER) {
            visitVar(node);
        } else if (t == TokenType.FUNCTIONCALL) {
            visitFuncCall(node);
        }else if(t == TokenType.ASSIGN){
            visitAssign(node);
        }else if(t == TokenType.PLUSEQUALSE
                || t == TokenType.MINUSEQUALSE
                || t == TokenType.MULEQUALSE
                || t == TokenType.DIVEQUALSE
                || t == TokenType.REMEQUALE){

        }else {
            //TUK NZ SKIV 6oto e ( expression ) kak tam,
            visitExpression(node);
        }
    }

    private void visitCompound(Node node) {
        for (Node n : node.getArrayList()) {
            if (n.getToken().getType() == TokenType.ASSIGN) {
                visitCheckAssignOrVarDelc(n);
            } else if (n.getToken().getType() == TokenType.FUNCTIONCREATE) {
                visitFuncDeclaration(n);
            } else {
                visitStatement(n);
            }
        }
    }

    private void visitCheckAssignOrVarDelc(Node node) {
        if (node.getLeftNode().getType().equals("")) {
            visitAssign(node);
        } else {
            visitVarDelc(node);
        }
    }

    private void visitAssign(Node node) {
        String name = node.getLeftNode().getTokenValue();
        Symbol varSymbol = scope.get(name);
        if (varSymbol == null) {
            throw new IllegalArgumentException("Cound NOT find " + name + " in the Symbow Table! visitAssign");
        }
        visitExpression(node.getRightNode());
    }

    private void visitVar(Node node) {
        String name = node.getTokenValue();
        Symbol varSymbol = scope.get(name);
        if (varSymbol == null) {
            if (name.equals("EMPTY")) {
                throw new IllegalArgumentException("NULL at " +
                        "at Column: " + node.getToken().getColumn() + " line: " + node.getToken().getLine());
            }
            throw new IllegalArgumentException("Cound NOT find " + name + " in the Symbow Table! visitVar" +
                    "at Column: " + node.getToken().getColumn() + " line: " + node.getToken().getLine());
        }
    }

    private void visitVarDelc(Node node) {
        String type = node.getLeftNode().getType();
        Symbol typeSymbol = scope.get(type);
        if (typeSymbol == null) {
            throw new IllegalArgumentException("Cound NOT find " + type + " in the Symbow Table!");
        }
        String varName = node.getLeftNode().getTokenValue();
        if (scope.contains(varName)) {
            throw new IllegalArgumentException(varName + " is already declared!");
        }
        scope.put(varName, typeSymbol);
        if (node.getRightNode() != null) {
            visitExpression(node.getRightNode());
        }
    }

    private void visitFuncDeclaration(Node node) {

    }

    private void visitFuncCall(Node node) {
        if(node.getTokenValue().equals("scanf")){

        }else{
        TokenType tokenRightNode = node.getRightNode().getToken().getType();
        if(node.getTokenValue().equals("printf")) {
            if (tokenRightNode == TokenType.IDENTIFIER) {
                visitVar(node.getRightNode());
            } else if (tokenRightNode == TokenType.STRING) {
                visitString();
            } else if (tokenRightNode == TokenType.INTEGER_LITERAL) {
                visitNumber(node.getRightNode());
            } else if (tokenRightNode == TokenType.ASSIGN) {
                visitAssign(node.getRightNode());
                visitVar(node.getRightNode().getLeftNode());
            } else if (tokenRightNode == TokenType.FUNCTIONCALL) {
                visitFuncCall(node.getRightNode());
            } else {
                visitOperator(node);
            }
        }else if(node.getTokenValue().equals("sqr") || node.getTokenValue().equals("abs") || node.getTokenValue().equals("odd")){
            if (tokenRightNode == TokenType.IDENTIFIER) {
                visitVar(node.getRightNode());
            }else if (tokenRightNode == TokenType.INTEGER_LITERAL) {
                visitNumber(node.getRightNode());
            } else if (tokenRightNode == TokenType.ASSIGN) {
                visitAssign(node.getRightNode());
                visitVar(node.getRightNode().getLeftNode());
            } else if (tokenRightNode == TokenType.FUNCTIONCALL) {
                visitFuncCall(node.getRightNode());
            } else {
                visitOperator(node);
            }
        }}
    }

    private void visitString() {

    }

    public void print() {
        System.out.println("HASHMAP _-----------------------");
        for (String name : scope.getHashMap().keySet()) {
            System.out.println(name + " <" + scope.get(name).getType() + ">");
        }
        System.out.println("HASHMAP _-----------------------");
    }

    public void visitProgram(Node node) {
        visitStatement(node);
    }
}
