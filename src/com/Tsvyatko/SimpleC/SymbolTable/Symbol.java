package com.Tsvyatko.SimpleC.SymbolTable;

public class Symbol {

	private String type;
	private boolean isConst = false;
	
	public Symbol(String type) {
		this.type = type;
	}
	
	public Symbol() {
		this.type = null;
	}

	public String getType() {
		return type;
	}

	public void makeItAConst(){
		isConst = true;
	}

	public boolean isConst() {
		return isConst;
	}
}
