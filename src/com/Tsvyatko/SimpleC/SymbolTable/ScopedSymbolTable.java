package com.Tsvyatko.SimpleC.SymbolTable;

import java.util.HashMap;
import java.util.Map;

public class ScopedSymbolTable {

	private Map<String, Symbol> symbols;
	private String name;
	private int level;
	
	private void initBuildInTypes() {
		symbols.put("int", new Symbol("int"));
		symbols.put("double", new Symbol("double"));
	}
	
	public ScopedSymbolTable(String name, int level) {
		symbols = new HashMap<>();
		this.name = name;
		this.level = level;
		initBuildInTypes();
	}
	
	public void put(String name, Symbol symbol) {
		//System.out.println("Define " + symbol.getName());
		symbols.put(name, symbol);
	}
	
	public Symbol get(String name) {
		//System.out.println("Lookup " + name);
		return symbols.get(name);
	}
	
	public boolean contains(String name) {
		return symbols.containsKey(name);
	}
	
	public Map<String, Symbol> getHashMap(){
		return symbols;
	}
}
