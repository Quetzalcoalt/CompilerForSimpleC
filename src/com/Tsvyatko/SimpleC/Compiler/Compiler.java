package com.Tsvyatko.SimpleC.Compiler;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.Tsvyatko.SimpleC.Main.Node;
import com.Tsvyatko.SimpleC.tokenizer.Token;
import com.Tsvyatko.SimpleC.tokenizer.TokenType;
import com.Tsvyatko.SimpleC.tokenizer.Type;
import jdk.internal.org.objectweb.asm.ClassWriter;
import jdk.internal.org.objectweb.asm.Label;
import jdk.internal.org.objectweb.asm.MethodVisitor;

import static jdk.internal.org.objectweb.asm.Opcodes.*;

public class Compiler {

    Label end = new Label();
    private Map<Integer, Label> labelListForIfStatement;
    private int labelCounter;
    private Map<String, String> variablesTable;
    private Map<String, Integer> varTable;
    private ArrayList<Type> variablesTableTypes;
    private Node AST;
    private int globalIndexCounter = 0;
    private String nameOfFile;

    public Compiler(Node node, String nameOfFile) {
        this.nameOfFile = nameOfFile;
        variablesTable = new HashMap<>();
        variablesTableTypes = new ArrayList<>();
        varTable = new HashMap<>();
        labelListForIfStatement = new HashMap<>();
        labelCounter = 0;
        AST = node;
    }

    public void compileBegin() {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES | ClassWriter.COMPUTE_MAXS);
        MethodVisitor mv;
        cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, nameOfFile, null,
                "java/lang/Object", null);

        cw.visitSource(nameOfFile + ".java", null);

        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            mv.visitLabel(l0);
            mv.visitLineNumber(4, l0);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>",
                    "()V", false);
            mv.visitInsn(RETURN);
            Label l1 = new Label();
            mv.visitLabel(l1);
            mv.visitLocalVariable("this", "Lhello/HelloWorld;", null, l0, l1, 0);
            mv.visitMaxs(1, 1);
            mv.visitEnd();
        }
        {
            mv = cw.visitMethod(ACC_PUBLIC + ACC_STATIC, "main",
                    "([Ljava/lang/String;)V", null, null);
            mv.visitCode();
            Label l0 = new Label();
            labelListForIfStatement.put(labelCounter, l0);
            labelCounter++;
            mv.visitLabel(l0);
            mv.visitLineNumber(7, l0);
            //         mv.visitLocalVariable("args", "[Ljava/lang/String;", null, l0, l1, 0);
            mv.visitLineNumber(8, l0);
            mv.visitTypeInsn(NEW, "java/util/Scanner");
            mv.visitInsn(DUP);
            mv.visitFieldInsn(GETSTATIC, "java/lang/System", "in", "Ljava/io/InputStream;");
            mv.visitMethodInsn(INVOKESPECIAL, "java/util/Scanner", "<init>", "(Ljava/io/InputStream;)V");
            //          mv.visitLocalVariable("s", "Ljava/util/Scanner;", null, l0, l1, 1);

            globalIndexCounter++;
            varTable.put("scanner", globalIndexCounter);
            mv.visitVarInsn(ASTORE, globalIndexCounter);
            globalIndexCounter++;

            Label l1 = new Label();
            labelListForIfStatement.put(labelCounter, l1);
            labelCounter++;
            mv.visitLabel(l1);

            compile(AST, mv);

            mv.visitInsn(RETURN);

            mv.visitLabel(end);
            //mv.visitInsn(RETURN);
            mv.visitMaxs(1000, 1000);
            mv.visitEnd();
        }

        System.out.println("Compiling finished");

        cw.visitEnd();

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(nameOfFile + ".class");
            fos.write(cw.toByteArray());
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void compile(Node node, MethodVisitor mv) {
        if (node != null) {
            visitStatement(node, mv); // remove getValue and return a node, process else where jsut in case of errors
        } else {
            throw new IllegalArgumentException("AST is NULL!");
        }
    }

    private void visitOperator(Node node, MethodVisitor mv) {
        if (node.getTokenValue().equals("+")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(IADD);
        }
        if (node.getTokenValue().equals("-")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(ISUB);
        }
        if (node.getTokenValue().equals("*")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(IMUL);
        }
        if (node.getTokenValue().equals("/")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(IDIV);
        }
        if (node.getTokenValue().equals("div")) {

        }
        if (node.getTokenValue().equals("%")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(IREM);
        }
        if (node.getTokenValue().equals("+=")) {
            //--------------------------------------------------------------------------------------------------
            //TEST THIS
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(IADD);
        }
        if (node.getTokenValue().equals("-=")) {
            visitExpression(node.getLeftNode(), mv);
            visitExpression(node.getRightNode(), mv);
            mv.visitInsn(ISUB);
        }
    }

    public void visitStatement(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        if (token == null) {
            throw new IllegalStateException("node token is NULL!");
        }
        if (token.getType() == TokenType.COMPOUND) {
            System.out.println("Opening a new COMPOUND");
            visitCompound(node, mv);
        } else if (token.getType() == TokenType.IF) {
            System.out.println("Opening a new IF");
            visitIF(node, mv);
            mv.visitLabel(new Label());
        } else if (token.getType() == TokenType.WHILE) {
            System.out.println("Opening a new WHILE");
            visitWhile(node, mv);
        } else if (token.getType() == TokenType.DO) {
            System.out.println("Opening a new DO");
            visitDoWhile(node, mv);
        }
        else {
            visitExpression(node, mv);
        }

    }

    private void visitIF(Node node, MethodVisitor mv) {
        int dummyCounter;
        //Label l2 = new Label();
        labelListForIfStatement.put(labelCounter, new Label());
        System.out.println("if create and visit " + labelCounter);
        mv.visitLabel(labelListForIfStatement.get(labelCounter));
        labelCounter++;
        //Label l3 = new Label();
        labelListForIfStatement.put(labelCounter, new Label());
        dummyCounter = labelCounter;
        labelCounter++;
        System.out.println("If create " + dummyCounter);
        visitExpression(node.getLeftNode(), mv);
        visitStatement(node.getRightNode(),mv);

        mv.visitLabel(labelListForIfStatement.get(dummyCounter));
        System.out.println("IF visiting last" + dummyCounter);
/* ELSE
        if(node.getElse() != null){
            //gtaserfgtsaergsergwsergsergasefgasssssssssssssssssswsergsergasefgasssssssssssssssssswsergsergasefgasssssssssssssssssswsergsergasefgassssssssssssssssss
            labelListForIfStatement.put(labelCounter, new Label());
            dummyCounter = labelCounter;
            labelCounter++;
            System.out.println("IF else label " + dummyCounter);
            visitStatement(node.getElse().getRightNode(),mv);
        }
*/
    }
    private void visitWhile(Node node, MethodVisitor mv) {
        if(node.getRightNode().getToken().getType() == TokenType.OPOSITE){
            node = node.getRightNode();
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;
            //Label l3 = new Label();
            labelListForIfStatement.put(labelCounter, new Label());
            System.out.println("While create Label " + labelCounter);

            int dummyCounter = labelCounter;
            labelCounter++;
            //goto L3
            mv.visitJumpInsn(GOTO, labelListForIfStatement.get(dummyCounter));
            System.out.println("While jump " + dummyCounter);

            //l6
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;

            visitStatement(node.getRightNode(),mv);

            //visit l3
            mv.visitLabel(labelListForIfStatement.get(dummyCounter));
            System.out.println("While visit " + dummyCounter);
            //labelcounter 7 => 6
            dummyCounter++;
            visitExpressionWhileOposite(node.getLeftNode(), mv, dummyCounter);

            //visit l4
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;
        }else{
            //Label l2 = new Label();
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;
            //Label l3 = new Label();
            labelListForIfStatement.put(labelCounter, new Label());
            System.out.println("While create Label " + labelCounter);

            int dummyCounter = labelCounter;
            labelCounter++;
            //goto L3
            mv.visitJumpInsn(GOTO, labelListForIfStatement.get(dummyCounter));
            System.out.println("While jump " + dummyCounter);

            //l6
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;

            visitStatement(node.getRightNode(),mv);

            //visit l3
            mv.visitLabel(labelListForIfStatement.get(dummyCounter));
            System.out.println("While visit " + dummyCounter);
            //labelcounter 7 => 6
            dummyCounter++;
            visitExpressionWhile(node.getLeftNode(), mv, dummyCounter);

            //visit l4
            labelListForIfStatement.put(labelCounter, new Label());
            mv.visitLabel(labelListForIfStatement.get(labelCounter));
            System.out.println("While create and visit " + labelCounter);
            labelCounter++;
        }

    }

    private void visitDoWhile(Node node, MethodVisitor mv) {
        //Label l2 = new Label();
        labelListForIfStatement.put(labelCounter, new Label());
        mv.visitLabel(labelListForIfStatement.get(labelCounter));
        labelCounter++;
        //Label l3 = new Label();
        mv.visitFrame(F_APPEND, 2, new Object[]{INTEGER, INTEGER}, 0, null);
        visitStatement(node.getRightNode(),mv);
        labelListForIfStatement.put(labelCounter, new Label());
        mv.visitLabel(labelListForIfStatement.get(labelCounter));
        visitExpressionDoWhile(node.getLeftNode(), mv);
        labelCounter++;
    }

    private void visitExpressionWhileOposite(Node node, MethodVisitor mv, int dummyCounter){
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.LESS
                || t == TokenType.LESSEQUALE
                || t == TokenType.GREATER
                || t == TokenType.GREATEREQUALE
                || t == TokenType.NOTEQUALE
                || t == TokenType.EQUALEEQUALE) {
            visitAdditiveExpression(node.getLeftNode(), mv);
            visitAdditiveExpression(node.getRightNode(), mv);
            if (t == TokenType.LESS) {
                mv.visitJumpInsn(IF_ICMPGE, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.GREATER) {
                mv.visitJumpInsn(IF_ICMPLE, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.LESSEQUALE) {
                mv.visitJumpInsn(IF_ICMPGT, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.GREATEREQUALE) {
                mv.visitJumpInsn(IF_ICMPLT, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.NOTEQUALE) {
                mv.visitJumpInsn(IF_ICMPEQ, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.EQUALEEQUALE) {
                mv.visitJumpInsn(IF_ICMPNE, labelListForIfStatement.get(dummyCounter));
            } else {
                error();
            }
        } else {
            visitAdditiveExpression(node, mv);
        }
    }

    private void visitExpressionDoWhile(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.LESS
                || t == TokenType.LESSEQUALE
                || t == TokenType.GREATER
                || t == TokenType.GREATEREQUALE
                || t == TokenType.NOTEQUALE
                || t == TokenType.EQUALEEQUALE) {
            visitAdditiveExpression(node.getLeftNode(), mv);
            visitAdditiveExpression(node.getRightNode(), mv);
            if (t == TokenType.LESS) {
                mv.visitJumpInsn(IF_ICMPLT, labelListForIfStatement.get(labelCounter-1));
            } else if (t == TokenType.GREATER) {
                mv.visitJumpInsn(IF_ICMPGT, labelListForIfStatement.get(labelCounter-1));
            } else if (t == TokenType.LESSEQUALE) {
                mv.visitJumpInsn(IF_ICMPLE, labelListForIfStatement.get(labelCounter-1));
            } else if (t == TokenType.GREATEREQUALE) {
                mv.visitJumpInsn(IF_ICMPGE, labelListForIfStatement.get(labelCounter-1));
            } else if (t == TokenType.NOTEQUALE) {
                mv.visitJumpInsn(IF_ICMPNE, labelListForIfStatement.get(labelCounter-1));
            } else if (t == TokenType.EQUALEEQUALE) {
                mv.visitJumpInsn(IF_ICMPEQ, labelListForIfStatement.get(labelCounter-1));
            } else {
                error();
            }
        } else {
            visitAdditiveExpression(node, mv);
        }
    }

    private void visitExpressionWhile(Node node, MethodVisitor mv, int dummyCounter) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.LESS
                || t == TokenType.LESSEQUALE
                || t == TokenType.GREATER
                || t == TokenType.GREATEREQUALE
                || t == TokenType.NOTEQUALE
                || t == TokenType.EQUALEEQUALE) {
            visitAdditiveExpression(node.getLeftNode(), mv);
            visitAdditiveExpression(node.getRightNode(), mv);
            if (t == TokenType.LESS) {
                mv.visitJumpInsn(IF_ICMPLT, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.GREATER) {
                mv.visitJumpInsn(IF_ICMPGT, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.LESSEQUALE) {
                mv.visitJumpInsn(IF_ICMPLE, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.GREATEREQUALE) {
                mv.visitJumpInsn(IF_ICMPGE, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.NOTEQUALE) {
                mv.visitJumpInsn(IF_ICMPNE, labelListForIfStatement.get(dummyCounter));
            } else if (t == TokenType.EQUALEEQUALE) {
                mv.visitJumpInsn(IF_ICMPEQ, labelListForIfStatement.get(dummyCounter));
            } else {
                error();
            }
        } else {
            visitAdditiveExpression(node, mv);
        }
    }

    private void visitExpression(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.LESS
                || t == TokenType.LESSEQUALE
                || t == TokenType.GREATER
                || t == TokenType.GREATEREQUALE
                || t == TokenType.NOTEQUALE
                || t == TokenType.EQUALEEQUALE) {
            visitAdditiveExpression(node.getLeftNode(), mv);
            visitAdditiveExpression(node.getRightNode(), mv);
            if(t == TokenType.LESS){
                mv.visitJumpInsn(IF_ICMPGE, labelListForIfStatement.get(labelCounter-1));
            }else if(t == TokenType.GREATER){
                mv.visitJumpInsn(IF_ICMPLE, labelListForIfStatement.get(labelCounter-1));
            }else if(t == TokenType.LESSEQUALE){
                mv.visitJumpInsn(IF_ICMPGT, labelListForIfStatement.get(labelCounter-1));
            }else if(t == TokenType.GREATEREQUALE){
                mv.visitJumpInsn(IF_ICMPLT, labelListForIfStatement.get(labelCounter-1));
            }else if(t == TokenType.NOTEQUALE){
                mv.visitJumpInsn(IF_ICMPEQ, labelListForIfStatement.get(labelCounter-1));
            }else if(t == TokenType.EQUALEEQUALE){
                mv.visitJumpInsn(IF_ICMPNE, labelListForIfStatement.get(labelCounter-1));
            }else{
                error();
            }
        } else {
            visitAdditiveExpression(node, mv);
        }
        /*
        if(node.getToken().getType() == TokenType.INTEGER_LITERAL){
            visitNumber(node, mv);
        }else if (token.getType() == TokenType.PLUS || token.getType() == TokenType.MINUS
                || token.getType() == TokenType.DIV || token.getType() == TokenType.MUL || token.getType() == TokenType.DIV_INT
                || token.getType() == TokenType.REM
            //|| token.getType() == TokenType.PLUSEQUALSE || token.getType() == TokenType.MINUSEQUALSE
            //|| token.getType() == TokenType.MULEQUALSE || token.getType() == TokenType.DIVEQUALSE
                ) {
            visitOperator(node, mv);
        } else if (token.getType() == TokenType.ASSIGN) {
            visitAssign(node, mv);
        } else if (token.getType() == TokenType.IDENTIFIER) {
            visitVar(node, mv);
        }
        */
    }

    private void visitAdditiveExpression(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.PLUS) {
            visitMultiplicativeExpression(node.getLeftNode(), mv);
            visitMultiplicativeExpression(node.getRightNode(), mv);
            mv.visitInsn(IADD);
            //instruction za + ili minus
        } else if (t == TokenType.MINUS) {
            visitMultiplicativeExpression(node.getLeftNode(), mv);
            visitMultiplicativeExpression(node.getRightNode(), mv);
            mv.visitInsn(ISUB);
            //instruction za + ili minus
        } else {
            visitMultiplicativeExpression(node, mv);
        }

    }

    private void visitMultiplicativeExpression(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.MUL) {
            visitSimpleExpression(node.getLeftNode(), mv);
            visitSimpleExpression(node.getRightNode(), mv);
            mv.visitInsn(IMUL);
        } else if (t == TokenType.DIV) {
            visitSimpleExpression(node.getLeftNode(), mv);
            visitSimpleExpression(node.getRightNode(), mv);
            mv.visitInsn(IDIV);
        } else if (t == TokenType.REM) {
            visitSimpleExpression(node.getLeftNode(), mv);
            visitSimpleExpression(node.getRightNode(), mv);
            mv.visitInsn(IREM);
        } else {
            visitSimpleExpression(node, mv);
        }
    }

    private void visitSimpleExpression(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        if (t == TokenType.ASSIGN) {
            visitAssign(node, mv);
        } else {
            visitPrimaryExpression(node, mv);
        }
    }

    private void visitPrimaryExpression(Node node, MethodVisitor mv) {
        Token token = node.getToken();
        TokenType t = token.getType();
        //if const,
        if (t == TokenType.INTEGER_LITERAL) {
            visitNumber(node, mv);
        } else if (t == TokenType.IDENTIFIER) {
            visitVar(node, mv);
        } else if (t == TokenType.FUNCTIONCALL) {
            visitFuncCall(node, mv);
        } else {
            //TUK NZ SKIV 6oto e ( expression ) kak tam,
            visitExpression(node, mv);
        }
    }

    private void visitNumber(Node node, MethodVisitor mv) {
        mv.visitLdcInsn(Integer.valueOf(node.getTokenValue()));
    }

    private void visitCompound(Node node, MethodVisitor mv) {
        for (Node n : node.getArrayList()) {

            if (n.getToken().getType() == TokenType.ASSIGN) {
                visitVarDeclaration(n, mv);
            } else if (n.getToken().getType() == TokenType.FUNCTIONCREATE) {
                visitFuncDecalration(n, mv);
            } else {
                visitStatement(n, mv);
            }
        }
    }

    private void visitVarDeclaration(Node node, MethodVisitor mv) {
        Type type = new Type(node.getLeftNode().getTokenValue(), node.getLeftNode().getType());//pri a nqma type
        String desc = getDesc(type);
        if (desc.equals("ERROR")) {
            visitAssign(node, mv);
        } else {
            varTable.put(type.getName(), globalIndexCounter);
            visitExpression(node.getRightNode(), mv);
            mv.visitVarInsn(ISTORE, globalIndexCounter);
            globalIndexCounter++;
        }

    }

    private void visitFuncDecalration(Node node, MethodVisitor mv) {

    }

    private void visitAssign(Node node, MethodVisitor mv) {
        String name = node.getLeftNode().getTokenValue();
        int index = varTable.get(name);
        visitExpression(node.getRightNode(), mv);
        mv.visitVarInsn(ISTORE, index);
    }

    private String getDesc(Type type) {
        if (type.getType().equals("int")) {
            return "I";
        }
        return "ERROR";
    }

    //get index
    private void visitVar(Node node, MethodVisitor mv) {
        String name = node.getTokenValue();
        int index = varTable.get(name);
        mv.visitVarInsn(ILOAD, index);
    }

    private void visitFuncCall(Node node, MethodVisitor mv) {
/* SCANF */
        if (node.getTokenValue().equals("scanf")) {
            int index = varTable.get("scanner");
            mv.visitVarInsn(ALOAD, index);
            mv.visitMethodInsn(INVOKEVIRTUAL, "java/util/Scanner", "nextInt",
                    "()I", false);
            //TODO finish this, proveri za dali ima lqv i desen, 6oto pri scanf ima samo tkoen
            //ako e samo token izvikai scanera tam nz, vij kak stava
            //proveri right node kakvo e, ako e edintificator print dolnoto
            //ako e expression mv.visitLdcInsn(Integer.valueOf(visit(node.getRightNode())));
/* PRINTF */
        } else if (node.getTokenValue().equals("printf")) {
            TokenType tokenRightNode = node.getRightNode().getToken().getType();
            mv.visitFieldInsn(GETSTATIC, "java/lang/System", "out",
                    "Ljava/io/PrintStream;");
            if(tokenRightNode == TokenType.IDENTIFIER) {
                String name = node.getRightNode().getTokenValue();
                int index = varTable.get(name);
                mv.visitVarInsn(ILOAD, index);
            }else if(tokenRightNode == TokenType.STRING){
                mv.visitLdcInsn(node.getRightNode().getTokenValue());
            }
            else if(tokenRightNode == TokenType.PLUS
                    || tokenRightNode == TokenType.MINUS
                    || tokenRightNode == TokenType.MUL
                    || tokenRightNode == TokenType.DIV
                    ) {
                visitOperator(node.getRightNode(), mv);
            }else if(tokenRightNode == TokenType.INTEGER_LITERAL){
                visitNumber(node.getRightNode(), mv);
            }else if(tokenRightNode == TokenType.ASSIGN){
                visitAssign(node.getRightNode(), mv);
                visitVar(node.getRightNode().getLeftNode(), mv);
            }else if(tokenRightNode == TokenType.FUNCTIONCALL){
                visitFuncCall(node.getRightNode(),mv);
            }
            if(tokenRightNode == TokenType.STRING){
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println",
                        "(Ljava/lang/String;)V", false);
            }else{
                mv.visitMethodInsn(INVOKEVIRTUAL, "java/io/PrintStream", "println",
                        "(I)V", false);
            }

        }
/* SQR */
        else if (node.getTokenValue().equals("sqr")) {
            if( node.getRightNode().getToken().getType() == TokenType.IDENTIFIER) {
                String name = node.getRightNode().getTokenValue();
                int index = varTable.get(name);
                mv.visitVarInsn(ILOAD, index);
            }else if(node.getRightNode().getToken().getType() == TokenType.PLUS
                    || node.getRightNode().getToken().getType() == TokenType.MINUS
                    || node.getRightNode().getToken().getType() == TokenType.MUL
                    || node.getRightNode().getToken().getType() == TokenType.DIV
                    ) {
                visitOperator(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.INTEGER_LITERAL) {
                visitNumber(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.ASSIGN) {
                visitAssign(node.getRightNode(), mv);
                visitVar(node.getRightNode().getLeftNode(), mv);
            }else if(node.getRightNode().getToken().getType() == TokenType.FUNCTIONCALL){
                visitFuncCall(node.getRightNode(),mv);
            }
            mv.visitInsn(I2D);
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "sqrt", "(D)D", false);
            mv.visitInsn(D2I);
        }else if (node.getTokenValue().equals("abs")) {
            if( node.getRightNode().getToken().getType() == TokenType.IDENTIFIER) {
                String name = node.getRightNode().getTokenValue();
                int index = varTable.get(name);
                mv.visitVarInsn(ILOAD, index);
            }else if(node.getRightNode().getToken().getType() == TokenType.PLUS
                    || node.getRightNode().getToken().getType() == TokenType.MINUS
                    || node.getRightNode().getToken().getType() == TokenType.MUL
                    || node.getRightNode().getToken().getType() == TokenType.DIV
                    ) {
                visitOperator(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.INTEGER_LITERAL) {
                visitNumber(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.ASSIGN) {
                visitAssign(node.getRightNode(), mv);
                visitVar(node.getRightNode().getLeftNode(), mv);
            }else if(node.getRightNode().getToken().getType() == TokenType.FUNCTIONCALL){
                visitFuncCall(node.getRightNode(),mv);
            }
            mv.visitMethodInsn(INVOKESTATIC, "java/lang/Math", "abs", "(I)I", false);
        }
        else if (node.getTokenValue().equals("odd")) {
            if( node.getRightNode().getToken().getType() == TokenType.IDENTIFIER) {
                String name = node.getRightNode().getTokenValue();
                int index = varTable.get(name);
                mv.visitVarInsn(ILOAD, index);
            }else if(node.getRightNode().getToken().getType() == TokenType.PLUS
                    || node.getRightNode().getToken().getType() == TokenType.MINUS
                    || node.getRightNode().getToken().getType() == TokenType.MUL
                    || node.getRightNode().getToken().getType() == TokenType.DIV
                    ) {
                visitOperator(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.INTEGER_LITERAL) {
                visitNumber(node.getRightNode(), mv);
            }else if (node.getRightNode().getToken().getType() == TokenType.ASSIGN) {
                visitAssign(node.getRightNode(), mv);
                visitVar(node.getRightNode().getLeftNode(), mv);
            }else if(node.getRightNode().getToken().getType() == TokenType.FUNCTIONCALL){
                visitFuncCall(node.getRightNode(),mv);
            }
            mv.visitLdcInsn(Integer.valueOf(2));
            mv.visitInsn(IREM);
        }
    }

    private void error() {
    }

}
